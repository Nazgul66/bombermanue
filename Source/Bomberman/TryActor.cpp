// Fill out your copyright notice in the Description page of Project Settings.

#include "Bomberman.h"
#include "TryActor.h"


// Sets default values
ATryActor::ATryActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATryActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATryActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

